# example.py for uwsgi
#
# uwsgi --http 0.0.0.0:9000 --wsgi-file example.py
#
import RPi.GPIO as GPIO
from flask import Flask, render_template, request

import time

app = Flask(__name__)

GPIO.setmode(GPIO.BOARD)
pins = {
	18: {
		'name':'GPIO 23',
		'state': GPIO.LOW
	}
}

for pin in pins:
	GPIO.setup(pin,GPIO.OUT)
	GPIO.output(pin, GPIO.LOW)


@app.route("/")
#def index():
#	return "<html><body><h1>Test site 2 running under Flask & ngnix</h1></body></html>"
#def application(env, start_response):
#	start_response('200 OK', [('Content-Type','text/html')])
#	return [b"Hello uWSGI"]

def main():
	for pin in pins:
		pins[pin]['state'] = GPIO.input(pin)
	templateData = {
		'pins': pins
	}
	return render_template('main.html', **templateData)
	
@app.route("/<changePin>/<action>")
def action(changePin, action):
	changePin = int(changePin)
	deviceName = pins[changePin]['name']
	
	if action == "on":
		GPIO.output(changePin, GPIO.HIGH)
		message = "Turned " + deviceName + " on."
	if action == "off":
		GPIO.output(changePin, GPIO.LOW)
		message = "Turned " + deviceName + " off."

	for pin in pins:
		pins[pin]['state'] = GPIO.input(pin)
		
	templateData = {
		'pins': pins
	}
	return render_template('main.html', **templateData)

if __name__ == "__main__":
  app.run(host='0.0.0.0',debug=True)
