## uwsgi --http :8888 --wsgi-file foobar_gpio.py

import RPi.GPIO as GPIO

def application(env, start_response):
	start_response('200 OK', [('Content-Type','text/html')])
	
	GPIO.cleanup()
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(18,GPIO.OUT)
	
	GPIO.output(18,True)
	
	return [b"Hello 3 uWSGI"]

	
