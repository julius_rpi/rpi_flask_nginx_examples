# reference : https://www.datacamp.com/tutorial/making-http-requests-in-python

import requests

# The API endpoint
url = "https://jsonplaceholder.typicode.com/posts/"


# Adding a payload
payload = {"id": [1, 2, 3], "userId":1}

# A get request to the API
response = requests.get(url, params=payload)

print("Request status: " + str(response.status_code))

# Print the response
response_json = response.json()

for i in response_json:
    print(i, "\n")
