# example.py for uwsgi
#
# uwsgi --http 0.0.0.0:9000 --wsgi-file example.py
#
from flask import Flask, render_template, request
from flask import jsonify
import algo

app = Flask(__name__)

@app.route("/")
def index():
	return "<html><body><h1>Test site 2 running under Flask & ngnix</h1></body></html>"

# http://0.0.0.0:9001/list?wd=123
@app.route("/list", methods=['GET'])
def _list1():
    a = request.args.get('wd')
    if a == None:
        return "Empty List"
    else:
        a = float(a)
        res = algo.pi_sqrt(a)
        return jsonify({'result': str(res)})

# http://0.0.0.0:9001/list/10
@app.route("/list/<list_id>")
def _list2(list_id):
    return "list" + str(list_id)

@app.route("/app")
def application(env, start_response):
	start_response('200 OK', [('Content-Type','text/html')])
	return [b"Hello uWSGI"]

if __name__ == "__main__":
  app.run(host='0.0.0.0',debug=True)
