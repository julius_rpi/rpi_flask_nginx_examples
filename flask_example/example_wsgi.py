# example.py for uwsgi
#
# uwsgi --http 0.0.0.0:9000 --wsgi-file example_wsgi.py
#
from flask import Flask


app = Flask(__name__)

@app.route("/")
def index():
	return "<html><body><h1>Test site 3 running under Flask & ngnix</h1></body></html>"
#def application(env, start_response):
#	start_response('200 OK', [('Content-Type','text/html')])
#	return [b"Hello 1 uWSGI"]
	
#@app.route("/app")
#def application2(env, start_response):
#	start_response('200 OK', [('Content-Type','text/html')])
#	return [b"Hello 2 uWSGI"]

if __name__ == "__main__":
  app.run(host='0.0.0.0',debug=True)
