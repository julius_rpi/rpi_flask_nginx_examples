import math

def pi_sqrt(a):
    return math.sqrt(a*math.pi)
