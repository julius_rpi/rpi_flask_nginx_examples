# reference : https://www.datacamp.com/tutorial/making-http-requests-in-python

import requests

# The API endpoint
url = "https://jsonplaceholder.typicode.com/posts/1"

# A GET request to the API
response = requests.get(url)

print("Request status: " + str(response.status_code))
#print(type(response.status_code))

# Print the response
response_json = response.json()
print(response_json)

